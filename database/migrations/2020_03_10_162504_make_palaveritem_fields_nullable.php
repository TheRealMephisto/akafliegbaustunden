<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePalaveritemFieldsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('palaverItems', function (Blueprint $table) {
            $table->string('status')->nullable()->change();
            $table->string('description')->nullable()->change();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('palaverItems', function (Blueprint $table) {
            $table->string('status')->nullable(false)->change();
            $table->string('description')->nullable(false)->change();


        });
    }
}
