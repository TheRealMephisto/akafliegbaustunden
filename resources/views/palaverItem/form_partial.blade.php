
<div class="form-group">
    {!!Form::label('title','Titel',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        {!!Form::text('title',NULL,['class' => 'form-control','required' => 'required'])!!}
    </div>
</div>


<div class="form-group">
    {!!Form::label('users','Verantwortlich',['class' => 'control-label col-sm-2', ])!!}
    <div class="col-sm-8 col-sm-offset-1">
        @if(isset($palaverItem))
        {!!Form::select('user_id[]',$users,$palaverItem->user()->pluck('users.id')->all(),['class' => 'selectpicker form-control','data-live-search'=>'true','multiple'=>'multiple','data-selected-text-format' =>'count','data-size'=> '12'])!!}
        @elseif(Auth::check()) <!-- Preselect authenticated user if new palaverItem is created -->
        {!!Form::select('user_id[]',$users,Auth::user()->id,['class' => 'selectpicker form-control','data-live-search'=>'true','multiple'=>'multiple','data-selected-text-format' =>'count','data-size'=> '12'])!!}
        @else
            {!!Form::select('user_id[]',$users,NULL,['class' => 'selectpicker form-control','data-live-search'=>'true','multiple'=>'multiple','data-selected-text-format' =>'count','data-size'=> '12'])!!}
        @endif

    </div>
</div>

<div class="form-group ">
    {!!Form::label('date','Termin/Deadline',['class' => 'control-label col-sm-2'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        <div class='input-group date' id='datetimepicker1'>
            {!!Form::text('date','',['class' => 'form-control col-sm-10'])!!}
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
        </div>
    </div>
    <script type="text/javascript">

        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: "YYYY-MM-DD HH:mm",
                @if(isset($palaverItem))
                date: moment("{{$palaverItem->date}}", "YYYY-MM-DD HH:mm"),
                @else
                //TODO Default Time 0:00
                date: moment()
                @endif
            });
        });
    </script>
</div>

<div class="form-group">
    {!!Form::label('no_deadline','Keine Deadline',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-5 col-sm-offset-1">
        @if(isset($palaverItem) && empty($palaverItem->date))
            {!!Form::checkbox('no_deadline','1',true)!!}
        @else
            {!!Form::checkbox('no_deadline','1')!!}
        @endif
    </div>
</div>


<div class="form-group">
    {!!Form::label('description','Beschreibung',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        {!!Form::textarea('description',NULL,['class' => 'form-control','rows'=>'3'])!!}
    </div>
</div>


<div class="form-group">
    {!!Form::label('status','Status',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        {!!Form::textarea('status',NULL,['class' => 'form-control','rows'=>'3'])!!}
    </div>
</div>

<div class="form-group">
    {!!Form::label('project','Projekt',['class' => 'control-label col-sm-2'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        @if(isset($selected))
            {!!Form::select('project_id',$projects,$selected,['class' => 'form-control col-sm-4','required' => 'required'])!!}
        @else
            {!!Form::select('project_id',$projects,null,['class' => 'form-control col-sm-4','required' => 'required'])!!}
        @endif
    </div>
</div>


<div class="form-group">
    {!!Form::label('done','Abgeschlossen',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-5 col-sm-offset-1">
        {!!Form::checkbox('done')!!}
    </div>
</div>

<div class="form-group">
    {!!Form::label('canceled','Eingestellt',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-5 col-sm-offset-1">
        {!!Form::checkbox('canceled')!!}
    </div>
</div>
<script>


    $('.selectpicker').selectpicker({
        size: 4,
        noneSelectedText: 'Keiner',
        countSelectedText: function (x, y) {
            return x + ' Mitglieder ausgewählt';
        }
    });


</script>
