@if ($errors->has($field_name))
    <span class="help-block">
                <strong class="text-danger">{{ $errors->first($field_name) }}</strong>
            </span>
@endif