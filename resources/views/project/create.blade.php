@extends('layouts.outer_form_partial')
@section('title','Projekt hinzufügen')
@section('head')

@endsection

@section('content')

@section('panel_heading','Projekt hinzufügen')
@section('panel_body')

    {!!  Form::open(['method'=>'post','url' => 'project/','class' => 'form-horizontal','role'=>'form']) !!}
    @include('project.form_partial')


    @include('layouts.form_buttons_partial')


    {!! Form::close() !!}
@endsection

