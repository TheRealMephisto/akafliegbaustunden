<!DOCTYPE html>
<html lang="de">
<head>
    <title>Palaver Protokoll</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    @yield('head')

    <style>
        td {
            padding: 5px !important;
        }

        .small {
            padding: 1px;
        }
    </style>

</head>
<body>

<!-- TODO Shrink borders -->
<div class="container" style="font-size: 65%; border-collapse: collapse">


    <h4>Palaver Zusammenfassung {{$date}}</h4>
    <!-- palaver Einträge -->
    <div class="col-xs-10 row">
        <table class="table table-bordered" style="border-collapse: inherit">
            <thead>
            <tr>
                <th>Projekt</th>
                <th>Aufgabe</th>
                <th>Deadline</th>
                <th>Zuständig</th>

            </tr>
            </thead>

            @foreach($projects as $project)

                <?php $first = true ?>
                @foreach($project->palaverItems as $palaverItem)
                    @if($first)
                        <tr>
                            <td rowspan="{{$project->palaverItems->count()}}"> {{$project->name}}
                                , {{$project->supervisor_string}}</td>
                            <td>{{$palaverItem->title}}</td>
                            <td>
                                @if(is_null($palaverItem->date))
                                    ---
                                @elseif(\Carbon\Carbon::parse($palaverItem->date)->hour==0)
                                    {{\Carbon\Carbon::parse($palaverItem->date)->toDateString()}}
                                @else
                                    {{\Carbon\Carbon::parse($palaverItem->date)->toDateTimeString()}}
                                @endif
                            </td>
                            <td>{{$palaverItem->responsible_users}}</td>
                        </tr>
                    @else
                        <tr>
                            <td>{{$palaverItem->title}}</td>
                            <td>
                                @if(is_null($palaverItem->date))
                                    Null
                                @elseif(\Carbon\Carbon::parse($palaverItem->date)->hour==0)
                                    {{\Carbon\Carbon::parse($palaverItem->date)->toDateString()}}
                                @else
                                    {{\Carbon\Carbon::parse($palaverItem->date)->toDateTimeString()}}
                                @endif
                            </td>
                            <td>{{$palaverItem->responsible_users}}</td>
                        </tr>
                    @endif
                    <?php $first = false ?>
                @endforeach
            @endforeach
        </table>
        Anmerkungen: {{$anmerkung}}

    </div>

    <!-- Anwesenheits tabelle -->
    <div class="col-xs-2">
        <table class="table table-bordered" style="border-collapse: inherit;">
            <thead>
            <tr>
                <th>Name</th>
                <th>Da</th>
            </tr>
            </thead>
            @foreach($users as $user)
                <tr class="small">
                    <td>{{$user->short_name}}</td>
                    <td class="col-xs-1">
                        @if($anwesende->contains($user->id))
                            <span class=" glyphicon glyphicon-ok"></span>
                        @elseif($entschuldigte->contains($user->id))
                            <span class=" glyphicon glyphicon-ok-circle"></span>
                        @else
                            <span class=" glyphicon glyphicon-remove"></span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <div>
