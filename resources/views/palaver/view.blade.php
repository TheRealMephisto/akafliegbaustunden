@extends('layouts.layout')
@section('content')
    <div class="col-md-12">
        <table class="table table-hover  table-bordered">
            <thead>
            <tr>
                <td>Name</td>
                <td>Anwesend</td>
            </tr>
            </thead>
            @foreach($users as $user)
                <tr>
                    <td><a href='/user/{{$user->id}}'>{{$user->full_name}}</a></td>
                    <td class="col-md-1">
                        @if($user->anwesend($palaver))
                            <span class=" glyphicon glyphicon-ok" aria-hidden="true"></span>
                        @elseif($user->entschuldigt($palaver))
                            <span class=" glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
                        @else
                            <span class=" glyphicon glyphicon-remove" aria-hidden="true"></span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection