@unless(\Carbon\Carbon::parse($date)->lte(\Carbon\Carbon::today())&&$cars->isEmpty())
    <div>
        <h4>Autos:</h4>
        <!-- Display Car -->
        <?php $counter=0?>
        @foreach($cars as $car)
            <?php $counter ++ ?>
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h4 style="display: inline-block;">{{$car->fahrer->short_name}}</h4>
                    @can('destroy',$car)
                        <form action="/car/{{$car->id}}" method="post" style="float: right;">
                            {{ csrf_field() }}
                            {{method_field('DELETE')}}
                            <button class="btn btn-danger" type="submit"
                                    onclick="return confirm('Bist du dir sicher?')">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>
                    @endcan
                    <h5 style="margin-top: 0">{{$car->comment}}</h5>
                </div>
                <div class="panel-body">
                    @foreach($car->mitfahrer as $mitfahrer)
                        @can('remove_mitfahrer',[$car,$mitfahrer])
                            <div class="clearfix">
                                {{$mitfahrer->short_name}}
                                <form action="/car/{{$car->id}}/{{$mitfahrer->id}}" method="post"
                                      style="float: right">
                                    {{ csrf_field() }}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger" type="submit"
                                            onclick="return confirm('Bist du dir sicher?')">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </form>
                            </div>
                        @else
                            <p>{{$mitfahrer->short_name}}</p>
                        @endcan
                    @endforeach
                <!-- Freie Plätze -->
                    @can('addMultiplePassengers',$car)
                    <!-- Admin/Fahrer Form -->
                        <form action="/car/{{$car->id}}" method="post" id="passenger_form{{$counter}}">
                            {{ csrf_field() }}
                            {{method_field('PUT')}}
                            @for($c=($car->mitfahrer->count()) ;$c<$car->seats;$c++)
                                <p class="col-md-12">
                                    <select class="form-control car_{{$car->id}} selectpicker" data-live-search="true"
                                            rows="2"
                                            name="IDs[]" cols="50" form="passenger_form{{$counter}}">
                                        <option value=""></option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->short_name}}</option>
                                        @endforeach
                                    </select>
                                </p>

                            @endfor
                            <button class="btn btn-primary" style="display: inline-block;" type="submit">
                                Speichern
                            </button>
                        </form>
                    @else
                        @can('addPassenger',$car)
                        <!-- Normaler User -->
                            <form action="/car/{{$car->id}}" method="post" style="display: inline-block">
                                {{ csrf_field() }}
                                {{method_field('PUT')}}
                                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                                <button class="btn btn-primary" style="display: inline-block;" type="submit">
                                    Ich fahre mit!
                                </button>
                            </form>
                            @endif
                        @endcan
                </div>
            </div>
        @endforeach
    </div>
    @can('create',[App\models\car::class,$date])
        <div>
            <div class="panel panel-default"> <!-- Neues Auto erstellen -->
                <div class="panel-heading">Ich fahre Auto</div>
                <div class="panel-body">

                    <form action="/car/create" method="post" id=carForm">
                        {{csrf_field()}}
                        <input type="hidden" name="date" value="{{$date}}">
                        @if(Auth::user()->is_admin)
                            <select class="form-control selectpicker car_form" data-live-search="true" id="user_id"
                                    name="user_id">
                                @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->short_name}}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="hidden" value="{{Auth::user()->id}}" id="user_id" name="user_id">
                        @endif
                        <input class="form-control car_form" placeholder="Sitzplätze" type="number" name="seats"
                               id="seats"/>
                        <textarea class="form-control car_form" placeholder="Kommentar" id="comment"
                                  name="comment"></textarea>
                        <button class="button btn-primary" type="submit">Speichern</button>
                    </form>
                </div>
            </div>
        </div>
    @endcan

    @push('js')
    $('.selectpicker, .car_form').selectpicker({
    size: 10,
    noneSelectedText: 'Frei',

    });
    @endpush
@endunless
