<table class="table table-bordered">
    <thead>
    <tr>
        <th>Name</th>
        <th>Flugzeug</th>
        <th>Kommentar</th>
        <th>Monatsbaustunden</th>
    </tr>
    </thead>

    @foreach($pilots as $pilot)
        <tr>
            <td>{{$pilot->user->short_name}}</td>
            <td>{{$pilot->airplane->name}}</td>
            <td>
                {{$pilot->comment}}
                @can('destroy',$pilot)
                <form action="/pilot/{{$pilot->id}}" method="post"
                      style="display: inherit; float: right">
                    {{ csrf_field() }}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger" style="float: right" type="submit"
                            onclick="return confirm('Eintrag wirklich löschen?')">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </form>
                @endcan
            </td>
            <td>{{Help::format_time($pilot->user->work_this_month)}}</td>
        </tr>
        @endforeach
        @can('create',[App\models\pilot::class,$date])
                <!-- User ist flugberechtigt -->
        <tr><!-- Zeile zum eingeben beginnt hier -->
            <form action="/pilot" method="post" id="pilot_form">
                <input type="hidden" name="date" value="{{$date}}">
                {{ csrf_field() }}
                <td><!-- User select -->
                    @if(Auth::user()->is_admin)
                        <select class="form-control selectpicker pilot_form" data-live-search="true" id="pilot"
                                name="user_id" form="pilot_form">
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->short_name}}</option>
                            @endforeach
                        </select>
                        @else   <!-- Flugberechtigt aber nicht admin -->
                        {{Auth::user()->short_name}}
                        <input id="pilot" name="user_id" type="hidden" value="{{Auth::id()}}">
                    @endif
                </td>
                <td><!-- Flugzeug select -->
                    <select class="form-control selectpicker pilot_form" data-live-search="true" id="airplane" name="project_id" form="pilot_form">
                        <!-- Flugzeug kann wg. foreign key constraint nicht null sein -->
                        @foreach($airplanes as $airplane)
                            @if($airplane->flightReady)
                                <option value="{{$airplane->id}}">{{$airplane->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </td>

                <td><!-- Kommentar eingeben -->
                    <input class="form-control pilot_form" name="comment"
                           id="comment"/>

                    <!-- Speichern -->
                    <button class="btn btn-success" type="submit" style="float:right;"
                            onclick="return confirm('Eintrag wirklich speichern?')">
                        <span class="glyphicon glyphicon-check"></span>
                    </button>
                </td>
            </form>
        </tr>
        @else
                <!-- user nicht flugberechtigt -->
        <tr>
            @if(\Carbon\Carbon::today()->gte(\Carbon\Carbon::parse($date)))
                <td colspan="3">


                    Datum liegt in der Vergangenheit

                </td>
            @endif
        </tr>
        @endcan
</table>
