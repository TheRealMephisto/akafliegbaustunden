<?php

namespace Tests\Feature;

use App\Http\Requests\entry_request;
use Doctrine\Instantiator\Exception\ExceptionInterface;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;


class entryRequestTest extends TestCase
{



    public function testEarlyDate()
    {
        $data = array(
            'date'                      => \Help::beginOfCurrentYear()->subDay(),
            'description'               => "some long description",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id,
            'hours'                     => 20,
            'minutes'                   => 20
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));

        try {
            $entry_request->validateResolved();
        }

        catch (ValidationException $e){
            self::assertArrayHasKey( 'date', $e->errors());
            // Keine anderen Fehler
            self::assertEquals(1,sizeof($e->errors()));
        }

    }

    public function testEarliestPossibleDate()
    {
        $data = array(
            'date'                      => \Help::beginOfCurrentYear(),
            'description'               => "some long description",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id,
            'hours'                     => 20,
            'minutes'                   => 20
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));

        //gibt Null zurück, wenn Daten gültig
        self::assertNull($entry_request->validateResolved());

    }

    public function testNoHoursOrMinutes()
    {
        // Missing hours field
        $data = array(
            'date'                      => \Help::beginOfCurrentYear(),
            'description'               => "some long description",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id,
            // Missing hours field
            'minutes'                   => 20
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));

        //gibt Null zurück, wenn Daten gültig
        self::assertNull($entry_request->validateResolved());

        $data = array(
            'date'                      => \Help::beginOfCurrentYear(),
            'description'               => "some long description",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id,
            // Missing minutes field
            'hours'                     => 2,
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));

        //gibt Null zurück, wenn Daten gültig
        self::assertNull($entry_request->validateResolved());

    }

    public function testFutureDate()
    {
        $data = array(
            'date'                      => \Carbon\Carbon::tomorrow(),
            'description'               => "some long description",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id,
            'hours'                     => 20,
            'minutes'                   => 20
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));

        try {
            $entry_request->validateResolved();
        }

        catch (ValidationException $e){
            self::assertArrayHasKey( 'date', $e->errors());
            // Keine anderen Fehler
            self::assertEquals(1,sizeof($e->errors()));
        }
    }



    public function testNoWorkTime()
    {
        $data = array(
            'date'                      => \Carbon\Carbon::today(),
            'description'               => "some long text",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));


        try {
            $entry_request->validateResolved();
        }

        catch (ValidationException $e){
            self::assertArrayHasKey( 'hours', $e->errors());
            self::assertArrayHasKey( 'minutes', $e->errors());
            // Keine anderen Fehler
            self::assertEquals(2,sizeof($e->errors()));
        }

    }

    public function testTooShortDescription()
    {
        $data = array(
            'date'                      => \Carbon\Carbon::today(),
            'description'               => "shortText",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id,
            'hours'                     => 20,
            'minutes'                   => 20
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));

        try {
            $entry_request->validateResolved();
        }

        catch (ValidationException $e){
            self::assertArrayHasKey( 'description', $e->errors());
            // Keine anderen Fehler
            self::assertEquals(1,sizeof($e->errors()));
        }

    }

    public function testZeroWorkTime()
    {
        $data = array(
            'date'                      => \Carbon\Carbon::today(),
            'description'               => "some long text",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id,
            'hours'                     => 0,
            'minutes'                   => 0
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));


        try {
            $entry_request->validateResolved();
        }

        catch (ValidationException $e){
            self::assertArrayHasKey( 'hours', $e->errors(), 'Willst du was eintragen musst du auch was machen');
            self::assertArrayHasKey( 'minutes', $e->errors(), 'Willst du was eintragen musst du auch was machen');

            // Keine anderen Fehler
            self::assertEquals(2,sizeof($e->errors()));
        }

    }

    public function test_non_numeric_time()
    {
        $data = array(
            'date'                      => \Carbon\Carbon::today(),
            'description'               => "very long description",
            'user_id'                   => \App\models\User::first()->id,
            'palaverItem_id'            => \App\models\palaverItem::first()->id,
            'hours'                     => 'abc',
            'minutes'                   => '123#'
        );

        $entry_request = entry_request::create('/entry', 'POST', $data);
        $entry_request ->setContainer(app());
        $entry_request->setRedirector(app('Illuminate\Routing\Redirector'));

        try {
            $entry_request->validateResolved();
        }

        catch (ValidationException $e){
            self::assertArrayHasKey( 'hours', $e->errors(), 'Bitte Zahl eingeben');
            self::assertArrayHasKey( 'minutes', $e->errors(), 'Bitte Zahl eingeben');

            // Keine anderen Fehler
            self::assertEquals(2,sizeof($e->errors()));
        }

    }

}
