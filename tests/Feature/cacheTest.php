<?php

namespace Tests\Feature;


use App\models\entry;
use App\models\palaver;
use App\models\palaverItem;
use Carbon\Carbon;
use Carbon\Factory;
use Tests\TestCase;
use Illuminate\Support\Facades\Cache;
use Mockery;

/*
 * This tests correct behaviour of caching the workThisYear, workThisMonth and workThisWeek attributes.
 * Instead of summing up all entries by all users the current sum is cached and updated once a new entry is saved
 * This greatly improves loading time for project overview pages
 *
 * The chaching itself is implemented in app/models/palaverItem.php
 */
class cacheTest extends TestCase
{


    public function testPalaverItemCacheHit()
    {
        // user to visit Webseite
        $user = factory(\App\models\User::class)->make();

        $palaverItem = palaverItem::inRandomOrder()->first();

        $value1 = random_int(0,2400000);
        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisWeek')->andReturn(True);
        Cache::shouldReceive("get")->with($palaverItem->id.'palaverItemWorkThisWeek')->andReturn($value1);

        $value2 = random_int(0,2400000);
        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisMonth')->andReturn(True);
        Cache::shouldReceive("get")->with($palaverItem->id.'palaverItemWorkThisMonth')->andReturn($value2);

        $value3 = random_int(0,2400000);
        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisYear')->andReturn(True);
        Cache::shouldReceive("get")->with($palaverItem->id.'palaverItemWorkThisYear')->andReturn($value3);
        
        $this->actingAs($user)->get('/palaverItem/'.$palaverItem->id)->assertSee(floor($value1/60))->assertSee(floor($value2/60))->assertSee(floor($value3/60));

    }

    public function testPalaverItemPartialCacheMiss()
    {
        // user to visit Webseite
        $user = factory(\App\models\User::class)->make();
        $user->admin = 0;

        $palaverItem = palaverItem::inRandomOrder()->first();


        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisWeek')->andReturn(False);
        Cache::shouldReceive("put")->with($palaverItem->id.'palaverItemWorkThisWeek', Mockery::any(),\DateTime::class);



        $value2 = random_int(0,2400000);
        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisMonth')->andReturn(True);
        Cache::shouldReceive("get")->with($palaverItem->id.'palaverItemWorkThisMonth')->andReturn($value2);

        $value3 = random_int(0,2400000);
        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisYear')->andReturn(True);
        Cache::shouldReceive("get")->with($palaverItem->id.'palaverItemWorkThisYear')->andReturn($value3);

        $this->actingAs($user)->get('/palaverItem/'.$palaverItem->id)->assertSee(floor($value2/60))->assertSee(floor($value3/60));




    }

    public function testPalaverItemFullCacheMiss()
    {
        // user to visit Webseite
        $user = factory(\App\models\User::class)->make();
        $user->admin = 0;
        $palaverItem = palaverItem::inRandomOrder()->first();


        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisWeek')->andReturn(False);
        Cache::shouldReceive("put")->with($palaverItem->id.'palaverItemWorkThisWeek', Mockery::any(), \DateTime::class);


        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisMonth')->andReturn(False);
        Cache::shouldReceive("put")->with($palaverItem->id.'palaverItemWorkThisMonth', Mockery::any(), \DateTime::class);


        Cache::shouldReceive("has")->with($palaverItem->id.'palaverItemWorkThisYear')->andReturn(False);
        Cache::shouldReceive("put")->with($palaverItem->id.'palaverItemWorkThisYear', Mockery::any(), \DateTime::class);

        $this->actingAs($user)->get('palaverItem/'.$palaverItem->id)->assertSee($palaverItem->title);


    }


    public function testClearCacheOnUpdate()
    {
        /* @var entry $entry */
        $entry = factory(entry::class)->make();
        $entry->date = Carbon::today()->toDateString();
        $random = palaverItem::inRandomOrder()->first();

        $entry->palaverItem()->associate($random);

        // once for save and once for delete
        Cache::shouldReceive('forget')->with($random->id.'palaverItemWorkThisWeek')->twice();
        Cache::shouldReceive('forget')->with($random->id.'palaverItemWorkThisMonth')->twice();
        Cache::shouldReceive('forget')->with($random->id.'palaverItemWorkThisYear')->twice();


        self::assertTrue($entry->save());
        self::assertTrue($entry->delete());

    }

    /* TODO: THIS TEST
    public function testTotalValueUpdated()
    {

        $random = palaverItem::inRandomOrder()->first();

        $total_time_w = $random->work_this_week;
        $total_time_m = $random->work_this_month;
        $total_time_y = $random->work_this_year;

        $entry = factory(entry::class)->make();

        $entry->date = Carbon::today()->hour(0)->toDateString();
        print('count '.$random->entries()->count());

        $time = random_int(5,300);
        $entry->work_time = $time;
        $entry->palaverItem_id=$random->id;

        self::assertTrue($entry->save());
        print('count '.$random->entries()->count());

        print('before '.$total_time_m.' ');
        print('random '.$entry->work_time.' ');
        print('date '.Carbon::parse($entry->date)->gte(Carbon::today()->startOfMonth()).' ');

        print('total '. $random->work_this_month);

        self::assertTrue(($total_time_m + $time) == $entry->palaverItem->work_this_month);
        self::assertTrue(($total_time_w + $time) == $entry->palaverItem->work_this_week);
        self::assertTrue(($total_time_y + $time) == $entry->palaverItem->work_this_year);

        self::assertTrue($entry->delete());

    }

    */



    public function testClearCacheOnDelete(){

        /* @var entry $entry */
        $entry = factory(entry::class)->make();
        $random = palaverItem::inRandomOrder()->first()->id;
        $entry->palaverItem_id=$random;
        $entry->save();

        Cache::shouldReceive('forget')->with($random.'palaverItemWorkThisWeek')->once();
        Cache::shouldReceive('forget')->with($random.'palaverItemWorkThisMonth')->once();
        Cache::shouldReceive('forget')->with($random.'palaverItemWorkThisYear')->once();

        self::assertTrue($entry->delete());


    }

}
