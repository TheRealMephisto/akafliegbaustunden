<?php

namespace Tests\Browser;

use App\models\status;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use App\models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{

    public function testPageLoad()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->assertSee('Nachname')
                ->assertSee("Passwort")
                ->assertSee('Datenschutzerklärung')
            ;
        });


    }

    public function testFailedLogin(){
        $this->browse(function (Browser $browser){
            $browser->visit('/login')
                ->type('last_name', 'gibts_nicht')
                ->type('password','123')
                ->press('Login')
                ->pause(3000)
                ->assertSee('Diese Zugangsdaten sind leider ungültig.')
            ;
        });
    }

    public function testDatenschutzerklaerung(){
        $this->browse(function (Browser $browser){
           $browser->visit('/datenschutz')
               ->assertSee("Datenschutzerklärung")
               ->assertSee('Diese Datenschutzerklärung soll die Nutzer dieser Website über die Art, den Umfang und den Zweck der Erhebung und Verwendung personenbezogener Daten durch den Websitebetreiber Sebastian Clermont informieren. Der Websitebetreiber nimmt Ihren Datenschutz sehr ernst und behandelt Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Vorschriften. Da durch neue Technologien und die ständige Weiterentwicklung dieser Webseite Änderungen an dieser Datenschutzerklärung vorgenommen werden können, empfehlen wir Ihnen sich die Datenschutzerklärung in regelmäßigen Abständen wieder durchzulesen. Definitionen der verwendeten Begriffe (z.B. “personenbezogene Daten” oder “Verarbeitung”) finden Sie in Art. 4 DSGVO.')
               ->assertSee('webmaster@sebastianclermont.de');
           ;
        });

    }

    public function testExMember(){
        $this->browse(function (Browser $browser){
            $ausgetreten_id = status::where('name', 'Ausgetreten')->first()->id;

            /* @var User $user */
            $user = factory(User::class)->create();
            $user->status_id=$ausgetreten_id;
            // we set the password to something we know to be able to test it
            $user->password=bcrypt(123456);
            $user->save();
            $browser
                ->visit('/login')
                ->type('last_name', $user->last_name)
                ->type('password',123456)
                ->press('Login')
                ->pause(3000)
                ->assertSee('Diese Zugangsdaten sind leider ungültig.')
            ;
            //verify there is no session
            $browser->visit('/entry/create')
                ->assertPathIs('/login');
            $user->delete();
        });
    }


    public function testValidMember(){
        $this->browse(function (Browser $browser){

            /* @var User $user */
            $user = factory(User::class)->make();
            $user->status_id=2;
            // we set the password to something we know to be able to test it
            $user->password=bcrypt(123456789);
            $user->save();
            $browser
                ->visit('login')
                ->type('last_name', strtolower($user->last_name))
                ->type('password',123456789)
                ->press('Login')
                ->pause(3000)
                ->assertSee('Arbeitszeit')
                ->assertSee('Eintrag erstellen')
                ->assertSee($user->full_name);

            $browser->assertAuthenticatedAs($user);

            //verify there is a session
            $browser->visit('/entry/create')
                ->assertSee('Eintrag erstellen');

            $user->delete();
        });
    }




}
