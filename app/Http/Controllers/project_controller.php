<?php

namespace App\Http\Controllers;

use App\models\project;
use App\models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class project_controller extends Controller
{
    public function __construct() {
        //$this->middleware('palaver_auth', ['except' => ['index', 'show']]);
    }


    public function create() {

        return view('project.create')->with('users', User::get()->pluck('full_name', 'id'));
    }

    public function store(Request $request) {

        $this->validate($request,
                ['name' => 'required|string|min:3|unique:projects,name',
                    //    'description' => 'required|min:10',
                ]);

        $project = new project();
        $project->name = $request->name;
        $project->description = $request->description;
        $project->user_id = $request->user_id;
        if (isset($request->isAirplane)) {
            $project->isAirplane = 1;
        } else {
            $project->isAirplane = 0;
        }

        $project->active = 1; //Neu angelegtes Projekt muss aktiv sein

        $this->authorize('store', $project);

        $success = $project->save();

        if ($success) {
            Session::flash('alert-success', 'Projekt angelegt');
        } else {
            Session::flash('alert-danger', 'Das hat nicht funktioniert');
        }

        if (isset($request->type)) {
            if ($request->type == 'p') {
                return redirect('/palaver_view');
            } else if ($request->type == 'np') {
                return redirect('/palaverItem');
            }
        }
        return redirect('/project/' . $project->id);


    }

    public function index(Request $request) {

        if($request->show_all){
            $projects = project::with('palaverItems', 'palaverItems.entries', 'supervisor')->get();
        }

        else {
            $projects = project::active()->with(['palaverItems' => function ($q) {
                return $q->active();
            }], 'palaverItems.entries', 'supervisor')->get();
        }

        $date= Carbon::today()->subMonthNoOverflow()->day(1);
        $date2 = Carbon::today()->subMonthNoOverflow()->endOfMonth();


        $query="SELECT users.first_name, users.nickname, SUM(entries.work_time) AS 'Zeit' FROM users JOIN entries ON entries.user_id=users.id WHERE entries.date >= '". $date->toDateString() ."' AND entries.date <='". $date2->toDateString() ."' GROUP BY users.id, users.first_name, users.nickname ORDER BY SUM(entries.work_time) DESC LIMIT 3";


        $top3=\DB::select($query);

        //dd($date,$date2,$top3);

        return view('project.index')->with('projects', $projects)->with('show_all',$request->show_all)->with('top3',$top3);
    }

    public function update(Request $request, project $project) {

        $this->authorize('update', $project);
        $this->validate($request,
                ['name'        => 'required|unique:projects,name,' . $project->id,
                    //   'description' => 'required|min:10',
                ]);

        $project->name = $request->name;
        $project->description = $request->description;

        $project->user_id = $request->user_id;



        if (isset($request->flightReady)) {
            $project->flightReady = 1;
        } else {
            $project->flightReady = 0;
        }
        if (!isset($request->active) && \Auth::user()->is_admin) {
            $project->active = 0;
        }

        if (!isset($request->isAirplane) && \Auth::user()->is_admin) {
            $project->isAirplane = 0;
        }
        if(isset($request->isAirplane) && \Auth::user()->is_admin){
            $project->isAirplane=1;
        }
        if (Auth::user()->is_admin || !isset($project)) {
            if (isset($request->active)) {
                $project->active = true;
            } else {
                $project->active = false;
            }
        }

        $success = $project->save();

        if ($success) {
            Session::flash('alert-success', 'Projekt gespeichert');
        } else {
            Session::flash('alert-danger', 'Das hat nicht funktioniert');
        }

        try {
            Session::flash('hook', $project->palaverItems->first()->id);
        } catch (\Exception $e) {
            \Log::alert("Noch keine PalaverItems für " . $project->name);
        }

        if (isset($request->type)) {
            if ($request->type == 'p') {
                return redirect('/palaver_view');
            } else if ($request->type == 'np') {
                return redirect('/palaverItem');
            }
        }

        return redirect('/project/'.$project->id);
    }

    public function show(Request $request,project $project ) {

        if($request->show_all){
            $project=$project->load('palaverItems','palaverItems.entries','palaverItems.user');
        }

        else{

            $project = $project->load(['palaverItems' => function ($q) {
                return $q->where('done', '=', 0)->where('canceled', '=', 0)->orWhere('updated_at', '>', Carbon::today()->subWeek()->toDateString());
//TODO check for correct project selection
            }]);
        }

        return view('project.view')->with('project',$project)->with('show_all',$request->show_all);
    }

    public function edit(project $project) {
        return view('project.edit')->with('project', $project)->with('users', User::get()->pluck('full_name', 'id'));
    }
}


