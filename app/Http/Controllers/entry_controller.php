<?php

namespace App\Http\Controllers;

use App\Http\Requests\entry_request;
use App\Http\Requests\store_entry_request;
use App\models\entry;
use App\models\project;
use App\models\user;
use Carbon\Carbon;
use Session;

class entry_controller extends Controller {

    public function __construct() {
        //  $this->middleware('admin_auth', ['only' => ['edit', 'update', 'destroy']]);
    }

    public function create() {
        $projects = project::active()->with(['palaverItems' => function ($query){
            return $query->where('canceled','0')->where('done',0);
        }])->get();

        return view('/entry/create')->with('users', User::NonResigned()->get()->pluck('full_name', 'id'))->with('projects', $projects);
    }

    public function store(entry_request $request) {

       if (empty($request->hours)) {
            $request->hours = 0;
        } else if (empty($request->minutes)) {
            $request->minutes = 0;
        }

        $entry = new entry();
        $entry->user_id = $request->user_id;

        $entry->work_time = (60 * $request->hours) + $request->minutes;
        $entry->date = Carbon::parse($request->date)->toDateString();
        $entry->palaverItem_id = $request->palaverItem_id;
        $entry->description = $request->description;

        $this->authorize('store',$entry);


        $succes = $entry->save(); //saves entries

        if (isset($request->helper_id)) {
            $entry->helpers()->attach($request->helper_id); //saves helpers
        }

        if ($succes) {
            Session::flash('alert-success', 'Eintrag gespeichert');
        } else {
            Session::flash('alert-danger', 'Das hat nicht funktioniert');

        }
        return back();
    }

    public function edit(Entry $entry) {

        $this->authorize('update', $entry);


        $projects = project::active()->with('PalaverItems')->get();
        return view('entry.edit')->with('entry', $entry->load('helpers'))->with('users', User::get()->pluck('full_name', 'id'))->with('projects', $projects);

    }


    public function update(entry_request $request, entry $entry) {
        if (($request->hours == "" && $request->minutes == "") || ($request->hours == 0 && $request->minutes == 0))  {
            Session::flash('alert-warning', 'Arbeitszeit muss größer als 0 sein');
            return back()->withInput();
        }
        if (empty($request->hours)) {
            $request->hours = 0;
        } else if (empty($request->minutes)) {
            $request->minutes = 0;
        }
        $entry->work_time = (60 * $request->hours) + $request->minutes;
        $entry->date = Carbon::parse($request->date)->toDateString();
        $entry->palaverItem_id = $request->palaverItem_id;
        $entry->user_id = $request->user_id;
        $entry->description = $request->description;

        $this->authorize('update', $entry);

        $succes = false;
        try {
            ($succes = $entry->save());
            $entry->helpers()->detach();

            $entry->helpers()->attach($request->helper_id); //saves helpers
        } catch (\Exception $e) {
            $succes = false;
        }

        if ($succes) {
            Session::flash('alert-success', 'Eintrag aktualisiert');
        } else {
            Session::flash('alert-danger', 'Das hat nicht funktioniert');
        }


        return redirect('/user/'.\Auth::id().'?edit=true');
    }

    public function destroy(entry $entry) {

        $this->authorize('destroy', $entry);

        $entry->delete();

        return back();
    }

    public function index(\Illuminate\Http\Request $request) {
        $year = \Help::currentYear();
        if (isset($request->year)) {
            $year = $request->year;
        }

        $entries = entry::year($year)->with('palaverItem')->with('user')->with('palaverItem.user')->with('helpers')->get();
        return view('entry.index')->with('entries', $entries)->with('year', $year)->with('edit', $request->edit);

    }
}


