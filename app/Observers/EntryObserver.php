<?php

namespace App\Observers;

use App\models\entry;
use Illuminate\Support\Facades\Cache;

class EntryObserver
{
    /**
     * Handle the entry "created" event.
     *
     * @param  \App\entry  $entry
     * @return void
     */
    public function created(entry $entry)
    {
        $this->clearPalaverItemTimeCache($entry);

    }

    /**
     * Handle the entry "updated" event.
     *
     * @param  \App\entry  $entry
     * @return void
     */
    public function updated(entry $entry)
    {
        $this->clearPalaverItemTimeCache($entry);

    }

    /**
     * Handle the entry "deleted" event.
     *
     * @param  \App\entry  $entry
     * @return void
     */
    public function deleted(entry $entry)
    {
        $this->clearPalaverItemTimeCache($entry);

    }

    /**
     * Handle the entry "restored" event.
     *
     * @param  \App\entry  $entry
     * @return void
     */
    public function restored(entry $entry)
    {
        $this->clearPalaverItemTimeCache($entry);
    }

    /**
     * Handle the entry "force deleted" event.
     *
     * @param  \App\entry  $entry
     * @return void
     */
    public function forceDeleted(entry $entry)
    {
        $this->clearPalaverItemTimeCache($entry);
    }

    public function clearPalaverItemTimeCache($entry)
    {
        Cache::forget($entry->palaverItem_id.'palaverItemWorkThisWeek');
        Cache::forget($entry->palaverItem_id.'palaverItemWorkThisMonth');
        Cache::forget($entry->palaverItem_id.'palaverItemWorkThisYear');
    }
}
