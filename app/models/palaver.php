<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\models\palaver
 *
 * @property integer $id
 * @property mixed $data
 * @property string $date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\User[] $anwesende
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\User[] $entschuldigte
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaver whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaver whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaver whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaver whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaver whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaver aktuell()
 * @mixin \Eloquent
 * @property mixed $anwesenheit
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaver  senheit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaver whereAnwesenheit($value)
 */
class palaver extends Model {
    protected $table = 'palaver';

    /**
     *
     * @return boolean if palaver without palaver_data is in database
     *
     */
    public static function isPalaver() {

        return !empty(palaver::whereNull('data')->get()->first());
    }

    public function anwesende() {
        return $this->belongsToMany('App\models\user', 'user_palaver_anwesend', 'palaver_id', 'user_id');
    }

    public function entschuldigte() {
        return $this->belongsToMany('App\models\user', 'user_palaver_entschuldigt', 'palaver_id', 'user_id');
    }

    /**
     * @param $query
     *
     * @return palaver wo bis jetzt nur Anwesenheit festgestellt wurde
     */
    public function scopeAktuell($query) {
        return $query->whereNull('data')->first();
    }
}
