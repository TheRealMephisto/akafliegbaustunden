<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\models\status
 *
 * @property integer $id
 * @property string $name
 * @property float $required_hrs
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\models\status whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\status whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\status whereRequiredHrs($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\status whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\status whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class status extends Model
{
    protected $table='statuses';
    protected $fillable = ['id', 'name', 'required_hrs', 'created_at', 'updated_at'];

    public function user() {
        return $this->belongsTo('App\models\user');
    }
}
