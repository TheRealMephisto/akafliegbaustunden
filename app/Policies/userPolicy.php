<?php

namespace App\Policies;

use App\models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class userPolicy {
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function index(User $user) {
        if($user->is_admin){
            return $this->allow();
        }

        if ($user->can_palaver) {
            return $this->allow();
        }
        return $this->deny('Du musst admin oder Schriftführer sein, um dies sehen zu können');
    }

    public function create(User $user) {
        return $this->store($user, $user);
    }

    public function store(User $user, User $newUser) {
        if($user->is_admin){
            return $this->allow();
        }
        return $this->deny("Nur Admin darf neue Nutzer hinzufügen");
    }

    public function update(User $user, User $updatedUser) {
        return $this->edit($user, $updatedUser);
    }

    public function edit(User $user1, User $user2) {
        if($user1->is_admin){
            return $this->allow();
        }
        if ($user1->id === $user2->id) {
            return $this->allow();
        }
        return $this->deny('Du darfst nur dein eigenes Profil bearbeiten');
    }

    public function show(User $user1, User $user2) {
        if($user1->is_admin){
            return $this->allow();
        }
        if ($user1->id === $user2->id || $user1->can_palaver) {
            return $this->allow();
        }
        return $this->deny('Du darfst nur dein eigenes Profil ansehen');
    }

    public function remove_mitfahrer(User $user, User $mitfahrer) {
        if($user->is_admin){
            return $this->allow();
        }
        return $mitfahrer->id === $user->id;

    }

}
