<?php

namespace App\Policies;

use App\models\palaverItem;
use App\models\project;
use App\models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class palaverItem_policy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {

    }

    public function create(User $user, $palaverItem, project $project=NULL) {

        if($user->is_admin){
            return $this->allow();
        }

        if(env('USER_CAN_CREATE_PALAVERITEMS')){
            return $this->allow();
        }
        elseif(empty($project)){
            return $this->deny();
        }
        else if($project->supervisor == $user){
            return $this->allow();
        }
        return $this->deny('Nur Admins oder Projektleiter dürfen neue Aufgaben erstellen');
    }

    public function store(User $user, palaverItem $palaverItem) {
        if($user->is_admin){
            return $this->allow();
        }
        if ($palaverItem->project->supervisor == $user){
            return $this->allow();
        }
        return $this->deny("Nur Admin oder Projektleiter dürfen Aufgaben erstellen");
}

    public function update(User $user, palaverItem $palaverItem) {
        if($user->is_admin){
            return $this->allow();
        }
        return $this->edit($user, $palaverItem);
    }

    public function edit(User $user, palaverItem $palaverItem) {
        if($user->is_admin){
            return $this->allow();
        }


        if($palaverItem->user->contains($user)||$user->palaver){
            return $this->allow();
        }
        else if (is_null($palaverItem->project->supervisor)){
            return $this->deny();
        }
        else if($palaverItem->project->supervisor->id == $user->id){
            return $this->allow();
        }
        return $this->deny('Nur die Verantwortlichen dürfen den Eintrag bearbeiten');

    }
}
