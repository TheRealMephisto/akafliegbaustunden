<?php
namespace App\helpers;

use App\models\palaverItem;
use App\models\project;
use App\models\user;
use Carbon\Carbon;


class my_statistics_helper{



    public static function getWorkHoursByProjekt($year) {
        $dates = \Help::getStartEndDates($year);
        $start = $dates[0]->toDateString();
        $end = $dates[1]->toDateString();


        $projects = project::with(['palaverItems.entries' => function ($query) use ($start, $end) {
            $query->where('entries.date', '>=', $start)->where('entries.date', '<=
            ', $end);
        }], 'palaverItems', ['entries' => function ($query) use ($start, $end) {
            $query->where('entries.date', '>=', $start)->where('entries.date', '<=', $end);
        }])->get();


        $totalwork = 0;
        foreach ($projects as $project) {
            $totalwork += $project->getTotalTime($year);
        }

        $table = "<table class='table'><thead><tr><td></td><td>Projekt</td><td>Stunden</td><td>Anteil</td></tr><thead></thead></tr>";

        foreach ($projects as $project) {

            \Log::info($project->getTotalTime($year));
            $table .= '
            <tr class="clickable" data-toggle="collapse" id="row' . $project->id . '" data-target=".row' . $project->id . '">
            <td style="cursor:pointer"><i class="glyphicon glyphicon-plus"></i></td>
            <td>' . $project->name . "</td>
            <td>" . \Help::format_time($project->getTotalTime($year)) . '</td>
            ';

            if ($totalwork != 0) {
                $table .= '<td>' . round(100 * ($project->getTotalTime($year) / $totalwork), 2) . '%</td>
                
            </tr>';
            } else {
                $table .= '<td> --- </td>
            </tr>';
            }

            foreach ($project->palaverItems as $palaverItem) {
                $table .= '
                <tr  class="collapse row' . $project->id . '">
                <td></td>
                <td>' . $palaverItem->title . '</td>
                <td>' . \Help::format_time($palaverItem->getTotalWorkTime($year)) . '</td>
                <td></td>
                </tr>';
            }
        }

        $table .= '</table>';
        return $table;
    }

    public static function getWorkHoursByPalaverItem($year) {
        $start=Carbon::create($year-1,11,1);
        $end=Carbon::create($year,10,31);
        $output="";

        $palaverItems=palaverItem::with('entries')->get();

        foreach($palaverItems as $palaverItem){
            $output.=$palaverItem->title.": ".\Help::format_time($palaverItem->totalWorkTime)."<br>";
        }

        echo $output;

    }

    public static function getAktiveWorkTime($year) {
        $aktive=user::where('active',1)->with('entries')->get();
        $sum=0;

        foreach($aktive as $aktiver){
            $sum+=$aktiver->getTotalWorkTimeAttribute($year);
        }

        return \Help::format_time($sum);
    }

    public static function getAnwaerterWorkTime($year) {

        $anwaerter=user::where('active',0)->with('entries')->get();
        $sum=0;

        foreach($anwaerter as $item){
            $sum+=$item->getTotalWorkTimeAttribute($year);
        }
        return \Help::format_time($sum);

    }

    public static function getWayAboveAverageUsers($year) {

        $users = user::with('entries', 'status')->get()->where('vacation', false);
        $sum=0;

        foreach($users as $user){
            $sum+=$user->getTotalWorkTimeAttribute($year);
        }

        $average=$sum/$users->count(); // in minutes
        $aboveAverage = $average+($average*0.5);

        $goodUsers=$users->filter(function(user $user) use ($aboveAverage,$year){
            return $user->getTotalWorkTimeAttribute($year) > $aboveAverage;
        });

        $table="<table class='table'><tr><td>Name</td><td>Stunden</td></tr>";
        foreach($goodUsers as $user){
            $table.="<tr><td>".$user->full_name."</td><td>".\Help::format_time($user->getTotalWorkTimeAttribute($year))."</td></tr>";
        }
        $table.="</table>";

        return $table;

    }

    public static function getWayBelowAverageUser($year) {

        $users = user::with('entries', 'status')->get()->where('vacation', false);
        $sum=0;

        foreach($users as $user){
            $sum+=$user->getTotalWorkTimeAttribute($year);
        }

        $average=$sum/$users->count(); // in minutes
        $belowAverage = $average - ($average * 0.5);

        $badUsers = $users->filter(function (user $user) use ($belowAverage, $year) {
            return $user->getTotalWorkTimeAttribute($year) < $belowAverage;
        });

        $table="<table class='table'><tr><td>Name</td><td>Stunden</td></tr>";
        foreach($badUsers as $user){
            $table.="<tr><td>".$user->full_name."</td><td>".\Help::format_time($user->getTotalWorkTimeAttribute($year))."</td></tr>";
        }
        $table.="</table>";

        return $table;

    }

    /**
     * @param project_id id von Projekt für das Helfer ausgegeben werden sollen
     */
    public static function getProjectHelpers($project_id) {

        $statistic = \DB::select("SELECT users.first_name, users.nickname, SUM(entries.work_time) AS time, projects.name AS project from users JOIN entries on users.id = entries.user_id 
            JOIN palaverItems on entries.palaverItem_id = palaverItems.id 
            JOIN projects on palaverItems.project_id = projects.id WHERE projects.id LIKE " . $project_id . " GROUP BY users.id,projects.id,users.first_name,users.nickname,projects.name
            ORDER BY time DESC");

        $output = "<table class=\" table table-bordered \"><thead><tr><td>Name</td><td>Zeit</td></tr>";

        foreach ($statistic as $stat) {
            $output .= "<tr><td>";
            if (empty($stat->nickname)) {
                $output .= $stat->first_name;
            } else {
                $output .= $stat->nickname;
            }
            $output .= "</td><td>";
            $output .= \Help::format_time($stat->time);
            $output .= "</td ></tr >";
        }
        $output .= "<tr></tr></thead></table>";
        return $output;
    }



}


?>