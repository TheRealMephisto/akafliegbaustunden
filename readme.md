[![pipeline status](https://gitlab.com/nichtmonti/akafliegbaustunden/badges/master/pipeline.svg)](https://gitlab.com/nichtmonti/akafliegbaustunden/commits/master)
         
 [![coverage report](https://gitlab.com/nichtmonti/akafliegbaustunden/badges/master/coverage.svg)](https://gitlab.com/nichtmonti/akafliegbaustunden/commits/master)
 
 
 This project facilitates tracking of tasks and subtasks within an organization, as well as tracking work time for individual users and projects.
 
 It is based upon the [Laravel](laravel.com) PHP framework.
 
 Additionally it helps coordinating flight operations by tracking the status of the airplanes and validating that pilots have contributed sufficiently to club operations to be eligible for using he clubs airplanes.
 
 It is currently used at the Akaflieg Darmstadt. 
 
 
 ## Installation
 
 ### Docker
 
 The Docker image comes with the application and an Apache Web Server.
 
 The configuration is performed using environment variables.
 In the directory `Docker` there is a reference docker-compose.yml file provided, showcasing all necessary environment
  variables for successfully running the application as well as a database server.
 
 `USING THE PROVIDED DATABASE IS NOT PRODUCTION READY` since the database storage lives inside the container and is therefore not persistent. 
 It is recommended to use the environment variables to point this application to your existing database instance.
 
 #### Explantion of Environment Variables
 ##### APP_URL
 The URL from which the application is to be accessed. It can include a port, if a non-standard port (80 or 443) is used.
 This is used when generating links for the website.
 
 Example Value: `https://my-website.com`
 
 ##### DB_HOST
 The MYSQL database host to connect to.
 
 ##### DB_PORT
 The port to connect to
 ##### DB_USERNAME
 The name of the database user
 
 ##### DB_PASSWORD
 THe password of the database user
 
 ##### SENTRY_LARAVEL_DSN
 URL for application monitoring using [Sentry](sentry.io)
 
 ##### BEGIN_YEAR_MONTH
 The month in which total work time for the year is reset. Integer ranging from 1-12 expected
 
 ##### ALLOW_PAST_YEARS
 Boolean (1 or 0) expected. If set to 1 a dropdown to show entries from past years is available 
 
 #### Application Key
To obtain a secure key it can either be provided via the environment variable `APP_KEY` or a new key can be generated inside the container using the command `php artisan key:generate`.
 
 #### Encrypted Connection
 The Docker image is designed to be used in combination with an SSL-terminating reverse proxy or load balancer.
 Therefore a connection is only supported via HTTP on port 80. In a future release a HTTPS connection will be provided.
 
 #### Getting started
 There are migration files provided for setting up the database. The migration process can be invoked by using the command
 
 `docker exec -it -u 0 MY_CONTAINER_NAME php artisan migrate`
 
 To create the initial admin user account the artisan command `create_root_user` is provided.
 It can be called from the docker host using the command. Answer the prompts to create your root user.
 
 ` docker exec -it -u 0 MY_CONTAINER NAME artisan create_root_user`
 
 The container currently does not include development features such as seeding the database with example data.
 
 ### Manual Installation
 
 Server Requirements: 

    PHP >= 7.1.3
    BCMath PHP Extension
    Ctype PHP Extension
    JSON PHP Extension
    Mbstring PHP Extension
    OpenSSL PHP Extension
    PDO PHP Extension
    Tokenizer PHP Extension
    XML PHP Extension
    npm

Remeber to uncomment the required modules in your php.ini file
 #### Setup Files and Database
 1. Clone the repository 
 2. Configure the database and App URL using the .env file. There is an .env_template provided
 4. Run `php composer.phar install` to install php dependencies
 3. Run `npm install prod` to intall frontend dependencies
  3. Run `npm run prod` to gemerate frontend files
 5. Run `php artisan migrate` to create the required tables in the database
 6. Run `php artisan key:generate` to generate app key
 
#### Setup Webserver
This step depends on the webserver. Apache and NGINX with php-fpm both are viable options.

 - Configure web server to point to the 'public' directory

For evaluation and testing purposes Laravel provides a **non production ready** web server. 
It can be started with the command `php artisan serve`.
 
 #### Access Web Gui
 7. Go to the database and manually create a status. Then create a user with admin permissions in the users table. **Password is stored in the bcrypt format**.
 The password field in the users table expects a password in bcrypt format. For example to have a user with the password '1234' set the password field to '$2a$10$Lhj.2vNrluJprdWhz/6BUuBttsLFpZI8oTFOpUyI4YiMof2lZFmRO'
 8. Go to the URL you chose for hosting
 9. Login as the user defined in the previous step. Identifier is the last name of the user. Use the password you set in the previous step. 
 10. Enjoy
 
 Optional:
 For evaluation the database can be filled with auto-generated data by the command 'php artisan db:seed'
 This also creates an admin user with the credentials admin:1234
 
  