FROM php:7.4.3-apache


# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    libzip-dev \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_mysql zip exif pcntl
#RUN docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/
RUN docker-php-ext-install gd

# Install npm
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get -y install nodejs

# Add user for laravel application
RUN groupadd -g 1000 www

RUN useradd -u 1000 -ms /bin/bash -g www www && chown -R www:www /var/www

# Change current user to

USER www
RUN pwd
RUN cd /var/www && git clone https://gitlab.com/nichtmonti/akafliegbaustunden.git

WORKDIR /var/www/akafliegbaustunden

RUN git checkout install_helper

# Speeds up composer install by about 10x
RUN php composer.phar global require hirak/prestissimo

RUN php composer.phar install --no-dev --prefer-dist

USER www

RUN npm install prod
RUN npm run prod
RUN npm audit fix

RUN chmod -R 775 storage && chmod -R 775 bootstrap/cache


RUN cd /var/www/akafliegbaustunden && cp .env_docker .env

ENV APACHE_DOCUMENT_ROOT=/var/www/akafliegbaustunden/public
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2/apache2.pid
ENV APACHE_RUN_USER www
ENV APACHE_RUN_GROUP www

USER root
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite

CMD ["apache2", "-DFOREGROUND"]
